package core

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

//ImageGallery ...
type ImageGallery struct {
	ID          primitive.ObjectID   `bson:"_id"`
	ForeignID   string               `bson:"fid"`
	Title       map[string]string    `bson:"ti"`
	Description map[string]string    `bson:"de,omitempty"`
	Tags        map[string][]string  `bson:"tg,omitempty"`
	Models      []primitive.ObjectID `bson:"mo,omitempty"`
	Rate        string               `bson:"ra,omitempty"`
	ImagePath   []string             `bson:"ip,omitempty"`
	ImageCount  int64                `bson:"ic,omitempty"`
	CreatedAt   time.Time            `bson:"ca,omitempty"`
}

//GetSupportedLanguages returns slice of supported langs
func GetSupportedLanguages() []string {
	return []string{"en", "es", "fr", "de"}
}
