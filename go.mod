module gitlab.com/ngads-main/core

go 1.12

require (
	github.com/stretchr/testify v1.4.0 // indirect
	go.mongodb.org/mongo-driver v1.2.0
)
